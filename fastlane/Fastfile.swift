// This file contains the fastlane.tools configuration
// You can find the documentation at https://docs.fastlane.tools
//
// For a list of all available actions, check out
//
//     https://docs.fastlane.tools/actions
//

import Foundation

class Fastfile: LaneFile
{
    func cleanAndTestLane() {
        var devices: [String]? = ["iPhone 13 Pro Max"]
        runTests(project: "bucketbudget.xcodeproj",
                 scheme: "bucketbudget",
                 devices: .userDefined(devices))
    }
    
    func deployToTestflightLane() {
        desc("DEPLOY TO TESTFLIGHT")
        
        let username = "jacob.ackerman145@gmail.com"
        let appIdentifier = "com.ackerman.bucketbudget"
        
        //To see the itune connect team id for all teams, run:
        //fastlane deliver -u <account_user_name>
        let itcTeamId = "P2E7BV8DN7"
        
        //get version and build number
        let xcodeproj = "bucketbudget.xcodeproj"
        let versionNumber = getVersionNumber(xcodeproj: .userDefined(xcodeproj))
        latestTestflightBuildNumber(
            appIdentifier: appIdentifier,
            username: .userDefined(username),
            version: .userDefined(versionNumber),
            teamId: .userDefined(itcTeamId)
        )
        
        let fastlaneContext = laneContext()
        
        //Incrementing build number
        let currentBuildNumber = fastlaneContext["LATEST_TESTFLIGHT_BUILD_NUMBER"] as! Int
        let newBuildNumber: Int = currentBuildNumber + 1
        incrementBuildNumber(
            buildNumber: .userDefined(String(newBuildNumber)),
            xcodeproj: .userDefined(xcodeproj)
        )

        //Build
        let scheme = "bucketbudget"
        buildIosApp(scheme: .userDefined(scheme))
        
        // Upload to TestFlight
        let appleId = "1547156569"
        let devPortalTeamId = "P2E7BV8DN7"
        uploadToTestflight(
            username: .userDefined(username),
            appIdentifier: .userDefined(appIdentifier),
            appleId: .userDefined(appleId),
            teamId: itcTeamId,
            devPortalTeamId: .userDefined(devPortalTeamId)
        )
    }
}

//zuxv-bndh-usuu-ckir
    
