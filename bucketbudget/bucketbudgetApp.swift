//
//  bucketbudgetApp.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 7/24/20.
//

import SwiftUI
import Firebase

@main
struct bucketbudgetApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(SessionStore.shared)
        }
    }
}


class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
//        SessionStore.shared.signOut()
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        SessionStore.shared.signOut()
    }
}
