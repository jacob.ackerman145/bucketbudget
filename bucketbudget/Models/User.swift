//
//  User.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/19/20.
//

class User {
    var uid: String
    var email: String?
    var displayName: String?

    init(uid: String, displayName: String?, email: String?) {
        self.uid = uid
        self.email = email
        self.displayName = displayName
    }
}
