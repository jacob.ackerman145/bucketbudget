//
//  KeychainConfig.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/22/20.
//

struct KeychainConfig {
    static let serviceName = "BucketBudgetApp"
    static let accessGroup: String? = nil
}
