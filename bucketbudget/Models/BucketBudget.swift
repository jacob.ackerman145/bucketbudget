//
//  TimeBucket.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/8/20.
//

import Foundation
import SwiftUI
import FirebaseFirestoreSwift

struct BucketBudget: Identifiable, Hashable {
    @DocumentID var id: String?
    
    var totalAmountString = ""
    var totalAmount: Int {
        Int(totalAmountString) ?? 0
    }
    var amountRemainingString = ""
    var amountRemaining: Int {
        Int(amountRemainingString) ?? 0
    }
    var title = ""
    var description = ""
    var userId = ""
    
    var color: Color = Color.random
    
    init(totalAmount: String = "0", amountRemaining: String = "", title: String = "", description: String = "", id: String? = nil, color: Color = Color.random) {
        self.totalAmountString = totalAmount
        self.amountRemainingString = amountRemaining.isEmpty ? totalAmount : amountRemaining
        self.title = title
        self.description = description
        self.id = id
    }
}

extension BucketBudget: Codable {
    enum CodingKeys: String, CodingKey {
        case totalAmount
        case amountRemaining
        case title
        case description
        case userId
        case id
        case color
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(totalAmountString, forKey: .totalAmount)
        try container.encode(amountRemainingString, forKey: .amountRemaining)
        try container.encode(title, forKey: .title)
        try container.encode(description, forKey: .description)
        try container.encode(userId, forKey: .userId)
        try container.encode(color.toHexString(), forKey: .color) //need to store color, probably as hex
        //https://gist.github.com/aChase55/da4b2ab8e093a12c3c82015ab455ce77
        //https://gist.github.com/yannickl/16f0ed38f0698d9a8ae7
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalAmountString = try values.decode(String.self, forKey: .totalAmount)
        amountRemainingString = try values.decode(String.self, forKey: .amountRemaining)
        title = try values.decode(String.self, forKey: .title)
        description = try values.decode(String.self, forKey: .description)
        userId = try values.decode(String.self, forKey: .userId)
        let colorString = try? values.decode(String.self, forKey: .color)
        if let colorString = colorString {
            color = Color(hexString: colorString)
        } else {
            color = Color.random
        }
        id = try values.decode(DocumentID.self, forKey: .id).wrappedValue
    }
}
