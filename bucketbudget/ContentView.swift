//
//  ContentView.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 7/24/20.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var session: SessionStore
    
    func getUser() {
        session.listen()
    }
    
    var body: some View {
        Group {
            if session.session != nil {
                BucketGridView(viewModel: BucketGridViewModel())
            } else {
                LoginView()
                    .environmentObject(session)
            }
        }
        .onAppear(perform: getUser)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(SessionStore.shared)
    }
}
