//
//  Color+Hex.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 1/10/21.
//  Obtained from https://gist.github.com/yannickl/16f0ed38f0698d9a8ae7

import Foundation
import SwiftUI

extension Color {
    init(hexString:String) {
        let scanner = Scanner(string: hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(UIColor(red:red, green:green, blue:blue, alpha:1.0))
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        UIColor(self).getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}

//extension String {
//    var hex: Int? {
//        return Int(self, radix: 16)
//    }
//}
//
//
//extension UIColor {
//    convenience init(hex: Int) {
//        self.init(hex: hex, a: 1.0)
//    }
//
//    convenience init(hex: Int, a: CGFloat) {
//        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
//    }
//
//    convenience init(r: Int, g: Int, b: Int) {
//        self.init(r: r, g: g, b: b, a: 1.0)
//    }
//
//    convenience init(r: Int, g: Int, b: Int, a: CGFloat) {
//        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
//    }
//
//    convenience init?(hexString: String) {
//        guard let hex = hexString.hex else {
//            return nil
//        }
//        self.init(hex: hex)
//    }
//}

