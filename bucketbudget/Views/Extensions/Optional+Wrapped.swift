//
//  Wrapped+Optional.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 1/5/21.
//

import Foundation
import SwiftUI


func ??<T>(binding: Binding<T?>, fallback: T) -> Binding<T> {
  return Binding(get: {
    binding.wrappedValue ?? fallback
  }, set: {
    binding.wrappedValue = $0
  })
}
