//
//  View+Hiding.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/19/20.
//

import SwiftUI

extension View {
    @ViewBuilder func isHidden(_ isHidden: Bool) -> some View {
        if isHidden {
            self.hidden()
        } else {
            self
        }
    }
}
