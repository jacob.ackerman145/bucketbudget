//
//  CreateAccountView.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/19/20.
//

import SwiftUI

struct CreateAccountView: View {
    @State private var email = ""
    @State private var password = ""
    @State var loading = false
    @State var error = false
    
    @EnvironmentObject var session: SessionStore
    
    private func createAccount() {
        loading = true
        error = false
        
        session.signUp(email: email,
                       password: password) { (result, error) in
            if error != nil {
                self.error = true
            } else {
                self.email = ""
                self.password = ""
            }
            
            self.loading = false
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Form {
                    TextField("Email:",
                              text: $email)
                    SecureField("Password:",
                                text: $password)
                    Spacer()
                    Button("Submit") {
                        createAccount()
                    }
                }
                
                ProgressView("Creating...")
                    .isHidden(!loading)
                    .ignoresSafeArea()
                    .scaleEffect(2, anchor: .center)
                
            }
            .navigationTitle("Create Account")
        }
    }
}

struct CreateAccountView_Previews: PreviewProvider {
    static var previews: some View {
        CreateAccountView()
    }
}
