//
//  LoginView.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/19/20.
//

import SwiftUI

struct LoginView: View {
    @State private var email = UserDefaults.standard.value(forKey: SessionConstants.appUserName) as? String ?? ""
    @State private var password = ""
    @State private var showingBiometricEnrollment = false
    @State var loading = false
    @State var error = false
    @AppStorage(SessionConstants.biometricAuthEnabled) var biometricAuthEnabled = false
    
    @EnvironmentObject var session: SessionStore
    
    private func login() {
        loading = true
        error = false
        
        session.signIn(email: email,
                       password: password) { (result, error) in
            if error != nil {
                self.error = true
            } else {
                self.email = ""
                self.password = ""
            }
            
            loading = false
        }
    }
    
    private func loginWithBiometicAuth() {
        loading = true
        error = false
        
        BiometricAuth.authenticateUser { message in
            guard message == nil else {
                print("crap")
                return
            }
            
            let username = UserDefaults.standard.value(forKey: SessionConstants.appUserName) as? String ?? ""
            do {
                let passwordItem = KeychainPasswordItem(service: KeychainConfig.serviceName,
                                                        account: username,
                                                        accessGroup: KeychainConfig.accessGroup)
                let password = try passwordItem.readPassword()
                
                session.signIn(email: username,
                               password: password) { (result, error) in
                    if error != nil {
                        self.error = true
                    } else {
                        self.email = ""
                        self.password = ""
                    }
                    
                    loading = false
                }
            } catch let error {
                print("Error Authenticating: \(error.localizedDescription)")
            }
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                ProgressView()
                    .isHidden(!loading)
                    .ignoresSafeArea()
                    .scaleEffect(2, anchor: .center)
                    .zIndex(1)
                
                VStack {
                    Form {
                        Section {
                            TextField("Email:", text: $email)
                            SecureField("Password:", text: $password)
                        }
                        .font(.title3)
                        
                        Section {
                            Button(action: {
                                login()
                            }, label: {
                                HStack {
                                    Spacer()
                                    Text("Submit")
                                    Spacer()
                                }
                            })
                        }
                        
                        if (BiometricAuth.canEvaluatePolicy()) {
                            HStack {
                                Spacer()
                                if (BiometricAuth.canEvaluatePolicy() && !biometricAuthEnabled) {
                                    HStack {
                                        Spacer()
                                        Button(action: {
                                            showingBiometricEnrollment = true
                                        }) {
                                            Label("", systemImage: BiometricAuth.biometricType() == BiometricType.faceID ? "faceid" : "touchid")
                                                .scaleEffect(2.0)
                                        }
                                        Spacer()
                                    }
                                }
                            }
                        }
                    }
                    .alert(isPresented: $error) {
                        Alert(title: Text("Failed to Log In"),
                              message: Text("Unable to authenticate your session with those credentials."),
                              dismissButton: .default(Text("Ok")))
                    }
                    
                    ZStack {
                        Color.green
                            .ignoresSafeArea()
                            .frame(idealWidth: .infinity,
                                   maxHeight: 55.0)
                        NavigationLink(
                            destination: CreateAccountView()) {
                            Text("New to BucketBudget? Create an Account!")
                                .foregroundColor(.white)
                                .fontWeight(.medium)
                        }
                    }
                }
            }
            .navigationTitle("Log In")
            .actionSheet(isPresented: $showingBiometricEnrollment, content: {
                ActionSheet(title: Text("Enable \(BiometricAuth.biometricType().rawValue)?"), message: Text("Enable \(BiometricAuth.biometricType().rawValue) allows us to better protect your information."), buttons: [
                    .default(Text("Enroll")) {
                        UserDefaults.standard.set(true, forKey: SessionConstants.biometricAuthEnabled)
                    },
                    .cancel()
                ])
            })
            .onAppear(perform: {
                if biometricAuthEnabled {
                    loginWithBiometicAuth()
                }
            })
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
