//
//  FaceIdEnroll.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/22/20.
//

import SwiftUI

struct BiometricEnrollmentView: View {
    private func enableBiometricAuth() {
        BiometricAuth.authenticateUser { message in
            guard message == nil else {
                print(message)
                return
            }
            
            UserDefaults.standard.set(true, forKey: SessionConstants.biometricAuthEnabled)
        }
    }
    
    var body: some View {
        ZStack {
            Color.blue.ignoresSafeArea()
            
            VStack {
                Text("Enable \(BiometricAuth.biometricType().rawValue) to better protect your information!")
                Spacer()
                Button("Enable") {
                    
                }
            }
        }
    }
}

struct FaceIdEnroll_Previews: PreviewProvider {
    static var previews: some View {
        BiometricEnrollmentView()
    }
}
