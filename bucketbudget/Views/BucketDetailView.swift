//
//  BucketDetail.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/8/20.
//

import SwiftUI

struct BucketDetailView: View {
    @ObservedObject var viewModel: BucketDetailViewModel
    
    init(viewModel: BucketDetailViewModel) {
        self.viewModel = viewModel
        
        UITableView.appearance().separatorStyle = .singleLine
        UITableView.appearance().separatorColor = .white
        UITableView.appearance().backgroundColor = UIColor.clear
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                ProgressView()
                    .isHidden(!viewModel.isLoading)
                    .ignoresSafeArea()
                    .scaleEffect(2, anchor: .center)
                    .zIndex(1)
                
                viewModel.color
                    .edgesIgnoringSafeArea(.all)
                    .opacity(0.85)
                VStack {
                    List {
                        HStack {
                            Text("Description:")
                            Spacer()
                            Text("\(viewModel.description)")
                        }
                        .listRowBackground(viewModel.color)
                        
                        HStack {
                            Text("Total Amount:")
                            Spacer()
                            Text("$\(viewModel.totalAmount)" as String)
                        }
                        .listRowBackground(viewModel.color)
                        
                        HStack {
                            Text("Amount Remaining:")
                            Spacer()
                            Text("$\(viewModel.amountRemaining)" as String)
                        }
                        .listRowBackground(viewModel.color)
                    }
                    .foregroundColor(.white)
                    .listStyle(GroupedListStyle())
                    
                    VStack(spacing: 30) {
                        TextField("How much did you spend?", text: $viewModel.amountToSubstractString)
                            .listRowBackground(viewModel.color.opacity(0.85))
                            .foregroundColor(.white)
                        
                        //                    Picker(selection: $viewModel.amountToSubstractString, label: Text("How much did you spend?"), content: {
                        //                        ForEach(Array(stride(from: 1, to: 8, by: 2)), id: \.self) {
                        //                          Text("$\($0)")
                        //                        }
                        //
                        //                        ForEach(Array(stride(from: 5, to: 100, by: 5)), id: \.self) {
                        //                          Text("$\($0)")
                        //                        }
                        //                    })
                        //                    .listRowBackground(viewModel.color)
                        //                    .pickerStyle(WheelPickerStyle())
                        
                        Button(action: viewModel.subtract, label: {
                            HStack {
                                Spacer()
                                Label("Apply Charge", systemImage: "dollarsign.circle.fill")
                                Spacer()
                            }
                        })
                            .listRowBackground(viewModel.color)
                        
                        Spacer()
                    }
                    .padding()
                    
                    VStack {
                        Spacer()
                        
                        Button(action: viewModel.deleteBucket, label: {
                            HStack {
                                Spacer()
                                Label("Delete Bucket", systemImage: "trash.fill")
                                Spacer()
                            }
                        })
                        .listRowBackground(viewModel.color)
                        
                        Spacer()
                    }
                }
            }
            .navigationBarTitle(Text(viewModel.title),
                                displayMode: .inline)
            .background(NavigationConfigurator { nc in
                nc.navigationBar.backgroundColor = UIColor(viewModel.color)
                nc.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
                nc.navigationBar.tintColor = .white
                nc.navigationBar.isTranslucent = true
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }
}

struct BucketDetail_Previews: PreviewProvider {
    static var previews: some View {
        BucketDetailView(viewModel: BucketDetailViewModel(bucket: BucketBudget(totalAmount: "10",
                                                                               amountRemaining: "10",
                                                                               title: "Stuff",
                                                                               description: "stuff",
                                                                               id: nil)))
    }
}
