//
//  BucketGrid.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/8/20.
//

import SwiftUI

struct BucketGridView: View {
    @ObservedObject var viewModel: BucketGridViewModel
    
    init(viewModel: BucketGridViewModel) {
        self.viewModel = viewModel
    }
    
    private var gridList = [GridItem(.fixed(350))]
    
    var body: some View {
        NavigationView {
            //            ZStack {
            //                ProgressView()
            //                    .isHidden(!viewModel.loading)
            //                    .ignoresSafeArea()
            //                    .scaleEffect(2, anchor: .center)
            //                    .zIndex(1)
            
            VStack(spacing: 5) {
                VStack {
                    Color(UIColor.systemBackground).frame(height: 1)
                    Color(white: 0.5).frame(height: 3)
                }
                
                RefreshableScrollView(refreshing: $viewModel.pullToRefreshLoading) {
                    LazyVGrid(columns: gridList,
                              alignment: .center) {
                        ForEach(viewModel.buckets, id: \.self) { bucket in
                            NavigationLink(destination: BucketDetailView(viewModel: BucketDetailViewModel(bucket: bucket))) {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 15, style: .continuous)
                                        .fill(bucket.color)
                                        .shadow(radius: 10)
                                    
                                    VStack {
                                        Text(bucket.title)
                                            .font(.largeTitle)
                                            .foregroundColor(.white)
                                        
                                        Text(bucket.description)
                                            .font(.body)
                                            .foregroundColor(.gray)
                                        
                                        Text("$\(bucket.amountRemaining)" as String)
                                            .font(.largeTitle)
                                            .foregroundColor(.white)
                                            .fontWeight(.black)
                                            .minimumScaleFactor(0.6)
                                            .padding()
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 16)
                                                    .stroke(Color.white, lineWidth: 3)
                                            )
                                    }
                                    .padding(20)
                                    .multilineTextAlignment(.center)
                                }
                            }
                        }
                        NavigationLink(
                            destination: AddABucketView(viewModel: AddBucketViewModel())) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 15, style: .continuous)
                                    .fill(Color.green)
                                    .shadow(radius: 10)
                                
                                Label("Add a Bucket", systemImage: "plus")
                                    .foregroundColor(.white)
                            }
                            .frame(maxWidth: .infinity, idealHeight: 100.0, alignment: .center)
                            .padding()
                        }
                              }
                    .navigationBarTitle(Text("Your Buckets"),
                                        displayMode: .large)
                }
            }
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Menu {
                        Section {
                            Button(action: viewModel.logoff) {
                                Label("Log Off", systemImage: "arrowshape.turn.up.backward.fill")
                            }
                        }
                    }
                    label: {
                        Label("", systemImage: "chevron.down")
                            .foregroundColor(.primary)
                    }
                }
            }
        }
        //        }
        .onAppear {
            viewModel.getBuckets()
        }
    }
}

public extension Color {
    static var random: Color {
        return Color(red: .random(in: 0...0.45),
                     green: .random(in: 0...0.45),
                     blue: .random(in: 0...0.45))
    }
}


struct BucketGrid_Previews: PreviewProvider {
    static var previews: some View {
        BucketGridView(viewModel: BucketGridViewModel())
    }
}
