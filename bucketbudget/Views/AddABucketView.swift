//
//  AddABucketView.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/11/20.
//

import SwiftUI

struct AddABucketView: View {
    @ObservedObject var viewModel: AddBucketViewModel
    @State private var bucket = BucketBudget()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    init(viewModel: AddBucketViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                ProgressView()
                    .isHidden(!viewModel.isLoading)
                    .ignoresSafeArea()
                    .scaleEffect(2, anchor: .center)
                    .zIndex(1)
                
                VStack {
                    Form {
                        Section {
                            TextField("Name: ", text: $viewModel.title)
                            TextField("Description: ", text: $viewModel.description)
                        }
                        
                        Section {
                            TextField("Total Amount: ", text: $viewModel.totalAmountString)
                        }
                        
                        Section {
                            ColorPicker("Color", selection: $viewModel.color)
                        }
                        
                        Section {
                            Button("Add Bucket") {
                                viewModel.addBucket()
                            }
                        }
                    }
                    .alert(isPresented: $viewModel.hasError) {
                        Alert(title: Text("Failed to Create Bucket"),
                              message: Text("Unable to make this Bucket right now, please try again later."),
                              dismissButton: .default(Text("Ok")))
                    }
                }
            }
            .navigationBarTitle("New Bucket")
        }
        .onReceive(viewModel.shouldDismissViewPublisher) { shouldDismissView in
            if (shouldDismissView) {
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct AddABucketView_Previews: PreviewProvider {
    static var previews: some View {
        AddABucketView(viewModel: AddBucketViewModel())
    }
}
