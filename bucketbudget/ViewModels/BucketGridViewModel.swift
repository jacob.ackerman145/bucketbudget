//
//  BucketGridViewModel.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/20/20.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift
import SwiftUI

class BucketGridViewModel: ObservableObject {
    var session = SessionStore.shared
    
    @Published var buckets: [BucketBudget] = []
    @Published var loading = false
    @Published var pullToRefreshLoading = false {
        didSet {
            if oldValue == false && pullToRefreshLoading == true {
                getBuckets()
            }
        }
    }
    
    lazy var db: Firestore = {
        Firestore.firestore()
    }()
    
    func logoff() {
        do {
            try Auth.auth().signOut()
        } catch {
            //unable to log out
        }
    }
    
    func getBuckets() {
        guard session.session != nil, let userId = Auth.auth().currentUser?.uid else { return }
        
        loading = true
        db.collection("Buckets").whereField("userId", isEqualTo: userId).getDocuments { (snapshot, error) in
            guard error == nil else {
                print("failed to obtain buckets")
                self.pullToRefreshLoading = false
                self.loading = false
                return
            }
            
            guard let buckets = snapshot?.documents else {
                print("no buckets found")
                self.pullToRefreshLoading = false
                self.loading = false
                return
            }
            
            self.buckets = buckets.compactMap { document -> BucketBudget? in
                return try? document.data(as: BucketBudget.self)
            }
            
            self.loading = false
            self.pullToRefreshLoading = false
        }
    }
}
