//
//  BucketDetailViewModel.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 1/1/21.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift
import SwiftUI

class BucketDetailViewModel: ObservableObject {
    var session = SessionStore.shared
    private var bucket: BucketBudget
    
    lazy var db: Firestore = {
        Firestore.firestore()
    }()
    
    var amountToSubtract = 0
    
    @Published var isLoading = false
    @Published var errorMessage: String?
    @Published var navigateTo: String?
    @Published var amountToSubstractString: String = "" {
        didSet {
            self.amountToSubtract = Int(amountToSubstractString.components(separatedBy: CharacterSet.alphanumerics.inverted).joined()) ?? 0
        }
    }
    
    init(bucket: BucketBudget) {
        self.bucket = bucket
    }
    
    var title: String {
        self.bucket.title
    }
    
    var amountRemaining: Int {
        self.bucket.amountRemaining
    }
    
    var totalAmount: Int {
        self.bucket.totalAmount
    }
    
    var color: Color {
        self.bucket.color
    }
    
    var description: String {
        self.bucket.description
    }
    
    func subtract() {
        guard session.session != nil,
              let _ = Auth.auth().currentUser?.uid,
              let id = bucket.id else { return }
        
        isLoading = true
        let newAmount = bucket.amountRemaining - amountToSubtract
        db.collection("Buckets").document(id).updateData([ "amountRemaining": "\(newAmount)" ]) { error in
            
            self.isLoading = false
            if error != nil {
                print("failed to update record")
            } else {
                self.navigateTo = "BucketGridView"
            }
        }
    }
    
    func deleteBucket() {
        guard session.session != nil,
              let _ = Auth.auth().currentUser?.uid,
              let id = bucket.id else { return }
        
        isLoading = true
        db.collection("Buckets").document(id).delete { error in
            guard error == nil else {
                self.isLoading = false
                self.errorMessage = "Unable to delete bucket at this time."
                return
            }
            
            self.isLoading = false
            self.navigateTo = "BucketGridView"
        }
    }
    
//    func updateBucketWithAmount(_ newAmount: Int) {
//        let newBucket = BucketBudget(totalAmount: bucket.totalAmountString,
//                                     amountRemaining: "\(newAmount)",
//                                     title: bucket.title,
//                                     description: bucket.description,
//                                     id: bucket.id)
//
//        guard session.session != nil,
//              let _ = Auth.auth().currentUser?.uid,
//              let id = bucket.id else { return }
//
//        do {
//            try db.collection("Buckets").document(id).setData(from: newBucket)
//        }
//        catch {
//            print(error)
//        }
//    }
}
