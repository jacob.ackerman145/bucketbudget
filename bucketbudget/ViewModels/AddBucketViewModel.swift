//
//  AddBucketViewModel.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 1/5/21.
//

import Firebase
import FirebaseFirestoreSwift
import SwiftUI
import Combine

class AddBucketViewModel: ObservableObject {
    @Published var description = ""
    @Published var title = ""
    @Published var totalAmountString = ""
    @Published var color = Color.random
    
    @Published var isLoading = false
    @Published var hasError = false
    var shouldDismissViewPublisher = PassthroughSubject<Bool, Never>()
    private var shouldDismissView = false {
        didSet {
            shouldDismissViewPublisher.send(shouldDismissView)
        }
    }
    
    var session = SessionStore.shared
    
    lazy var db: Firestore = {
        Firestore.firestore()
    }()
    
    func addBucket() {
        isLoading = true
        
        guard !totalAmountString.isEmpty,
              !title.isEmpty,
              !description.isEmpty else {
            print("Failed to add Bucket, missing data.")
            return
        }
        
        let bucket = BucketBudget(totalAmount: totalAmountString,
                                  amountRemaining: totalAmountString,
                                  title: title,
                                  description: description,
                                  color: color)
        
        guard session.session != nil, let userId = Auth.auth().currentUser?.uid else { return }
        var newBucket = bucket
        newBucket.userId = userId
        
        do {
            _ = try db.collection("Buckets").addDocument(from: newBucket)
            isLoading = false
            shouldDismissView = true
        } catch let error {
            hasError = true
            print("Failed to add Bucket: \(error.localizedDescription)")
            isLoading = false
        }
    }
}
