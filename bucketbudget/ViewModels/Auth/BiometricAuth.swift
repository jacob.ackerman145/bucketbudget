//
//  BiometricAuth.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/22/20.
//

import Foundation
import LocalAuthentication

enum BiometricAuth {
    static func canEvaluatePolicy() -> Bool {
        let context = LAContext()
        return context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                         error: nil)
    }
    
    static func biometricType() -> BiometricType {
        let context = LAContext()
        let _ = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                          error: nil)
        
        switch context.biometryType {
        case .none:
            return .none
        case .touchID:
            return .touchID
        case .faceID:
            return .faceID
        @unknown default:
            return .none
        }
    }
    
    static func authenticateUser(completion: @escaping (String?) -> Void) {
        guard BiometricAuth.canEvaluatePolicy() else {
            completion("Biometric authentication not available.")
            return
        }
        
        let context = LAContext()
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Logging in with \(BiometricAuth.biometricType().rawValue)") { success, error in
            
            if success {
                DispatchQueue.main.async {
                    completion(nil)
                }
            } else {
                let message: String
                
                switch error {
                case LAError.authenticationFailed?:
                    message = "Identity can not be verified."
                case LAError.userCancel?:
                    message = "User canceled."
                case LAError.userFallback?:
                    message = "User chose to user password."
                case LAError.biometryNotAvailable?:
                    message = "FaceID/TouchID is not available on this device."
                case LAError.biometryNotEnrolled?:
                    message = "FaceID/TouchID is not enrolled on this device."
                case LAError.biometryLockout?:
                    message = "FaceID/TouchID is locked out on this device."
                default:
                    message = "FaceID/TouchID may not be configured."
                }
                
                completion(message)
            }
        }
    }
}

public enum BiometricType: String {
    case none = ""
    case touchID = "TouchID"
    case faceID = "FaceID"
}
