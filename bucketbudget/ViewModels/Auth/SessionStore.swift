//
//  SessionStore.swift
//  bucketbudget
//
//  Created by Jacob Ackerman on 12/19/20.
//

import SwiftUI
import Firebase
import Combine

class SessionStore: ObservableObject {
    static let shared = SessionStore()
    
    private init() { }
    
    var didChange = PassthroughSubject<SessionStore, Never>()
    @Published var session: User? { didSet { self.didChange.send(self) }}
    var handle: AuthStateDidChangeListenerHandle?
    
    func listen() {
        // monitor authentication changes using firebase
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                // if we have a user, create a new user model
                print("Got user: \(user)")
                self.session = User(
                    uid: user.uid,
                    displayName: user.displayName,
                    email: user.email
                )
            } else {
                // if we don't have a user, set our session to nil
                self.session = nil
            }
        }
    }
    
    func signUp(
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback
    ) {
        Auth.auth().createUser(withEmail: email,
                               password: password) { result, error in
            UserDefaults.standard.set(email, forKey: SessionConstants.appUserName)
            
            let passwordItem = KeychainPasswordItem(service: KeychainConfig.serviceName,
                                                    account: email,
                                                    accessGroup: KeychainConfig.accessGroup)
            do {
                try passwordItem.savePassword(password)
            } catch let error {
                fatalError("Error Updating Keychain: \(error.localizedDescription)")
            }
            
            handler(result, error)
        }
    }
    
    func signIn(
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback
    ) {
        Auth.auth().signIn(withEmail: email,
                           password: password) { result, error in
            if UserDefaults.standard.value(forKey: SessionConstants.appUserName) == nil {
                UserDefaults.standard.set(email, forKey: SessionConstants.appUserName)
                
                let passwordItem = KeychainPasswordItem(service: KeychainConfig.serviceName,
                                                        account: email,
                                                        accessGroup: KeychainConfig.accessGroup)
                do {
                    try passwordItem.savePassword(password)
                } catch let error {
                    fatalError("Error Updating Keychain: \(error.localizedDescription)")
                }
            }
            
            handler(result, error)
        }
    }
    
    func signOut() -> Bool {
        do {
            try Auth.auth().signOut()
            self.session = nil
            return true
        } catch {
            return false
        }
    }
    
    func unbind() {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
}

public enum SessionConstants {
    static let appUserName = "BucketBudgetAppUsername"
    static let biometricAuthEnabled = "BiometricAuthEnabled"
}

